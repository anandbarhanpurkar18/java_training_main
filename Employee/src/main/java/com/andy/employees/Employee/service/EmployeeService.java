package com.andy.employees.Employee.service;

import com.andy.employees.Employee.repository.EmployeeRepository;
import com.andy.employees.Employee.entities.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Page<Employee> getAllEmployee(int PageNo,int PageSize) {
        Pageable pageable = PageRequest.of(PageNo,PageSize);
        Page<Employee> pagedResult = employeeRepository.findAll(pageable);
        return pagedResult;
    }
    public List<Employee> findAll(){
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(long id) {
        return employeeRepository.findById(id);
    }

    public Employee addEmployee(Employee e) {
        return employeeRepository.save(e);
    }

    public void removeEmployee(long id){
         employeeRepository.deleteById(id);
    }

    // changes to be made over here
    public void updateEmployee(Employee e) {
        employeeRepository.save(e);
    }

}
