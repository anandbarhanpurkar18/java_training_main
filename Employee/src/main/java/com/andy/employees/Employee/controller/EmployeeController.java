package com.andy.employees.Employee.controller;

import com.andy.employees.Employee.entities.Employee;
import com.andy.employees.Employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employee")
    public List<Employee> getAllEmployee()
    {
        return employeeService.findAll();
    }
    @GetMapping("/employee/{pageNo}/{pageSize}")
    public Page<Employee> getEmployee(@PathVariable int pageNo, @PathVariable int pageSize) {
        return employeeService.getAllEmployee(pageNo,pageSize);
    }

    @GetMapping("/employee/{id}")
    public Employee getEmployeeById(@PathVariable("id") Long id) {
        Employee employee= employeeService.getEmployeeById(id);
        if(employee==null)
        {
            throw new RuntimeException("Employee with the specified id is not present");
        }
        return employee;
    }

    @PostMapping("/employee")
    public Employee addEmployee(@RequestBody Employee employee) {
        return employeeService.addEmployee(employee);
    }

    @PutMapping("/employee")
    public void updateEmployee(@RequestBody Employee employee) {
         employeeService.updateEmployee(employee);
    }

    @DeleteMapping("/employee/{id}")
    public String deleteEmployee(@PathVariable("id") long id) {
        // To check if employee is present or not
        Employee employee = employeeService.getEmployeeById(id);
        if(employee==null)
            throw new RuntimeException("Employee is not present with the id you passed");
        String return_value="Employee deleted with the mentioned id";
        employeeService.removeEmployee(id);
        return  return_value;
    }

}
