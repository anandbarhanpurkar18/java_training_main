package com.andy.employees.Employee.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name="Employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long Id;

    private String firstName;

    private String lastName;

    private String fatherName;

    private String designation;

    private Gender gender;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Address> addresses=new ArrayList<>();



}
