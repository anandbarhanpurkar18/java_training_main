package com.andy.employees.Employee.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String aID;

    private String correspondenceAddress;

    private String permanentAddress;

    @ManyToOne
    @JoinColumn(name="employeeId")
    private Employee employeeId;

}
