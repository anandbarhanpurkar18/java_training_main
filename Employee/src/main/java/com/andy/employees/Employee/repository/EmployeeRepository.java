package com.andy.employees.Employee.repository;

import com.andy.employees.Employee.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {

    public Employee findById(long employeeid);

}
